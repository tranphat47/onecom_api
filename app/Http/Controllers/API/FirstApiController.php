<?php

namespace App\Http\Controllers\API;


use Illuminate\Http\Request;
use App\Http\Controllers\API\ResponseController as ResponseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\News;
use App\Registration;
use App\Role;
use App\Shiftwork;
use App\Team;

class FirstApiController extends ResponseController
{
    public $successStatus = 200;

    private $dir = 'http://huuhieu.name.vn/CheckinApi/public/uploads/';

    //----- LOGIN
    public function login(Request $request)
    {
        $userExist = User::where('username', $request['username'])->where('deleted', 0)->first();
        if (!$userExist) {
            $data['success'] = false;
            $data['message'] = "Failed! This user was deleted!";
        } else {
            if (Auth::attempt(['username' => request('username'), 'password' => request('password'), 'deleted' => 0])) {
                $data['success'] = true;
                $data['message'] = 'Đăng nhập thành công';
                $user = Auth::user();
                $success['token'] = $user->createToken('MyApp')->accessToken;

                //get user information
                $success['user_info'] = $this->getInfoUser($user);

                // get info my team
                $success['team_info'] = $this->getInfoTeam($user);

                // get list news in team
                $success['list_news'] = $this->getNews($user);

                // get all shiftworks info in my team
                $success['list_shiftwork'] = $this->getShiftwork($user);

                // get all users info in my team
                $success['list_user'] = $this->getListUser($user);

                // get my registration
                $success['my_registrations'] = $this->getRegistration($user);

                // get my registration
                $success['my_registrations_today'] = $this->getRegistrationToday($user);

                // get all lists
                $success['app_list'] = $this->getAppList($user);

                $data['data'] = $success;
            } else {
                $data['success'] = false;
                $data['message'] = 'Mật khẩu hoặc tên đăng nhập không chính xác';
                $data['data'] = [];
            }
        }
        return response()->json($data, $this->successStatus);

    }

    //----- LOGOUT
    public function logout(Request $request)
    {

        $isUser = $request->user()->token()->revoke();
        if ($isUser) {
            $success['message'] = "Successfully logged out.";
            return $this->sendResponse($success);
        } else {
            $error = "Something went wrong.";
            return $this->sendResponse($error);
        }


    }

    //----- USER
    public function createUser(Request $rq)
    {
//        $validator = Validator::make($rq->all(), [
//            'username' => 'required|string|unique:users',
//        ]);
//
//        if ($validator->fails()) {
//            $data['success'] = false;
//            $data['message'] = "Failed! This account already exists!";
//            return response()->json($data, $this->successStatus);
//        }
        $userExist = User::where('username', $rq['username'])->where('deleted', 0)->first();
        if ($userExist) {
            $data['success'] = false;
            $data['message'] = "Failed! User already exists";
        } else {
            $rq['id'] = substr(str_shuffle('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, 20);
            $rq['password'] = bcrypt($rq['password']);
            if ($rq['avataUrl'] != null) {
                $rq['avataUrl'] = $this->uploadImage($rq['id'], $rq['avataUrl'], 'created');
            }

            $user = User::create([
                'id' => $rq['id'],
                'firstName' => $rq['firstName'],
                'lastName' => $rq['lastName'],
                'dateOfBirth' => date('Y-m-d', strtotime($rq['dateOfBirth'])),
                'email' => $rq['email'],
                'phone' => $rq['phone'],
                'avataUrl' => $rq['avataUrl'],
                'jobTitle' => $rq['jobTitle'],
                'username' => $rq['username'],
                'password' => $rq['password'],
                'team_id' => $rq['team_id'],
            ]);

            if ($user) {
                $data['success'] = true;
                $data['message'] = "Successful!";
            } else {
                $data['success'] = false;
                $data['message'] = "Failed! Please try again!";
            }
        }
        return response()->json($data, $this->successStatus);
    }

    public function changePassword(Request $rq)
    {
        $user = $rq->user();
        if (Hash::check($rq['old_password'], $user['password'])) {
            $new_password = bcrypt($rq['new_password']);
            $user->update(['password' => $new_password]);
            $data['success'] = true;
            $data['message'] = 'Thay đổi mật khẩu thành công';
            return response()->json($data, $this->successStatus);
        }
        $data['success'] = false;
        $data['message'] = 'Mật khẩu cũ không chính xác';
        return response()->json($data, $this->successStatus);
    }

    public function updateUser(Request $rq)
    {
        if ($rq['avataUrl'] != null) {
            $rq['avataUrl'] = $this->uploadImage($rq['id'], $rq['avataUrl'], 'created');
        }
        $item = User::where('id', $rq['id'])->update($rq->all());
        if ($item) {
            $data['success'] = true;
            $data['message'] = "Successful!";
        } else {
            $data['success'] = false;
            $data['message'] = "Failed! Please try again!";
        }
        return response()->json($data, $this->successStatus);
    }

    public function updateTimeLimit(Request $request)
    {
        $item = Team::where('id', $request['id'])->update([
            'timeLimit' => $request['timeLimit'],
        ]);
        if ($item) {
            $data['success'] = true;
            $data['message'] = "Successful!";
            return response()->json($data, $this->successStatus);
        } else {
            $data['success'] = true;
            $data['message'] = "Failed! Please try again!";
            return response()->json($data, $this->successStatus);
        }
    }

    public function deleteUser(Request $rq)
    {
        $item = User::where('deleted', '=', 0)->where('id', '=', $rq['id'])->update([
            'deleted' => 1,
        ]);
        if ($item) {
            $data['success'] = true;
            $data['message'] = "Successful!";
        } else {
            $data['success'] = false;
            $data['message'] = "Failed! Please try again!";
        }
        return response()->json($data, $this->successStatus);
    }

    //----- REGISTRATION
    public function createRegistration(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'shiftwork_id' => 'required',
            'date' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->sendError($validator->errors());
        }

        $input = $request->all();
        $input['id'] = substr(str_shuffle('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, 20);
        $input['date'] = date('Y-m-d', strtotime($request['date']));
        $registration = Registration::create($input);
        if ($registration) {
            $data['success'] = true;
            $data['message'] = "Successful!";
        } else {
            $data['success'] = false;
            $data['message'] = "Failed! Please try again!";
        }
        return response()->json($data, $this->successStatus);
    }

    public function createRegistrationAllday(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'date' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->sendError($validator->errors());
        }
        $i = 0;
        $input = $request->all();
        $input['date'] = date('Y-m-d', strtotime($request['date']));
        $user = $request->user();
        $list = Shiftwork::where('deleted', '=', 0)->where('team_id', '=', $user->team_id)->where('shiftworkName', '<>', 'All day')->get();
        foreach ($list as $l) {
            $input['shiftwork_id'] = $l['id'];
            $input['id'] = substr(str_shuffle('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, 20);
            $registration = Registration::create($input);
            $i++;
        }
        if ($registration && $i == 2) {
            $data['success'] = true;
            $data['message'] = "Successful!";
        } else {
            $data['success'] = false;
            $data['message'] = "Failed! Please try again!";
        }
        return response()->json($data, $this->successStatus);
    }

    public function updateRegistration(Request $rq)
    {
        $item = Registration::where('id', $rq['id'])->update($rq->all());
        // $item->save();
        if ($item) {
            $data['success'] = true;
            $data['message'] = "Successful!";
        } else {
            $data['success'] = false;
            $data['message'] = "Failed! Please try again!";
        }
        return response()->json($data, $this->successStatus);
    }

    public function deleteRegistration(Request $rq)
    {
        $item = Registration::where('id', '=', $rq['id'])->delete();
        if ($item) {
            $data['success'] = true;
            $data['message'] = "Successful!";
        } else {
            $data['success'] = false;
            $data['message'] = "Failed! Please try again!";
        }
        return response()->json($data, $this->successStatus);
    }

    public function deleteRegistrationAllday(Request $rq)
    {
        $item = Registration::where('deleted', '=', 0)->where('user_id', '=', $rq['user_id'])->where('date', $rq['date'])->delete();
        if ($item) {
            $data['success'] = true;
            $data['message'] = "Successful!";
        } else {
            $data['success'] = false;
            $data['message'] = "Failed! Please try again!";
        }
        return response()->json($data, $this->successStatus);
    }
    //----- GET
    // get info 1 user
    private function getInfoUser(User $user)
    {
        $data['id'] = $user->id;
        $data['username'] = $user->username;
        $data['password'] = $user->password;
        $data['email'] = $user->email;
        $data['firstName'] = $user->firstName;
        $data['lastName'] = $user->lastName;
        $data['dateOfBirth'] = date("Y-m-d", strtotime($user->dateOfBirth));
        $data['phone'] = $user->phone;
        $data['avataUrl'] = $user->avataUrl;
        $data['team_id'] = $user->team_id;
        $data['jobTitle'] = $user->jobTitle;
        $data['role_id'] = $user->role_id;
        $data['created_at'] = date("H:i:s Y-m-d", strtotime($user->created_at));
        $data['updated_at'] = date("H:i:s Y-m-d", strtotime($user->updated_at));
        return $data;
    }

    // get news in team
    private function getNews(User $user)
    {
        $data = array();
        $list = News::where('deleted', '=', 0)->where('team_id', '=', $user->team_id)->orderBy('created_at', 'desc')->get();
        foreach ($list as $l) {
            $item['id'] = $l['id'];
            $item['title'] = $l['title'];
            $item['description'] = $l['description'];
            $item['url'] = $l['url'];
            $item['imgUrl'] = $l['imgUrl'];
            $item['createBy'] = $l['createBy'];
            $item['team_id'] = $l['team_id'];
            $item['created_at'] = date("H:i:s Y-m-d", strtotime($l['created_at']));
            $item['updated_at'] = date("H:i:s Y-m-d", strtotime($l['updated_at']));
            $data[] = $item;
        }
        return $data;
    }

    // get my registration
    private function getRegistration(User $user)
    {
        $data = array();
        $list = Registration::where('deleted', '=', 0)->where('user_id', '=', $user->id)->get();
        foreach ($list as $l) {
            $item['id'] = $l['id'];
            $item['user_id'] = $l['user_id'];
            $item['shiftwork_id'] = $l['shiftwork_id'];
            $item['shiftworkName'] = $l['shiftworkName'];
            $item['date'] = $l['date'];
            $item['isCheckin'] = strval($l['isCheckin']);
            $item['isCheckout'] = strval($l['isCheckout']);
            $item['isPresent'] = $l['isPresent'];
            $item['atCompany'] = $l['atCompany'];
            $item['created_at'] = date("H:i:s Y-m-d", strtotime($l['created_at']));
            $item['updated_at'] = date("H:i:s Y-m-d", strtotime($l['updated_at']));
            $data[] = $item;
        }
        return $data;
    }

    // get my registration on today
    private function getRegistrationToday(User $user)
    {
        $data = array();
        $list = Registration::where('deleted', '=', 0)->where('user_id', '=', $user->id)->where('date', date('Y-m-d'))->first();
        if (!empty($list['id'])) {
            $data['id'] = $list['id'];
            $data['user_id'] = $list['user_id'];
            $data['shiftwork_id'] = $list['shiftwork_id'];
            $data['shiftworkName'] = $list['shiftworkName'];
            $data['date'] = $list['date'];
            $data['isCheckin'] = $list['isCheckin'];
            $data['isCheckout'] = $list['isCheckout'];
            $data['isPresent'] = $list['isPresent'];
            $data['atCompany'] = $list['atCompany'];
            $data['created_at'] = date("H:i:s Y-m-d", strtotime($list['created_at']));
            $data['updated_at'] = date("H:i:s Y-m-d", strtotime($list['updated_at']));
        } else $data = null;
        return $data;
    }

    // get all shiftwork in my team
    private function getShiftwork(User $user)
    {
        $data = array();
        $list = Shiftwork::where('deleted', '=', 0)->where('team_id', '=', $user->team_id)->orderBy('created_at')->get();
        foreach ($list as $l) {
            $item['id'] = $l['id'];
            $item['shiftworkName'] = $l['shiftworkName'];
            $item['begin'] = date("H:i:s Y-m-d", strtotime($l['begin']));
            $item['end'] = date("H:i:s Y-m-d", strtotime($l['end']));
            $item['team_id'] = $l['team_id'];
            $item['created_at'] = date("H:i:s Y-m-d", strtotime($l['created_at']));
            $item['updated_at'] = date("H:i:s Y-m-d", strtotime($l['updated_at']));
            $data[] = $item;
        }
        return $data;
    }

    // get info my team
    private function getInfoTeam(User $user)
    {
        $data = array();
        $list = Team::where('deleted', '=', 0)->where('id', '=', $user->team_id)->get();
        foreach ($list as $l) {
            $item['id'] = $l['id'];
            $item['teamName'] = $l['teamName'];
            $item['teamCode'] = $l['teamCode'];
            $item['email'] = $l['email'];
            $item['phone'] = $l['phone'];
            $item['address'] = $l['address'];
	    $item['timeLimit'] = strval($l['timeLimit']);
            $item['isWorkOnSat'] = $l['isWorkOnSat'];
            $item['ipWifi'] = $l['ipWifi'];
            $item['created_at'] = date("H:i:s Y-m-d", strtotime($l['created_at']));
            $item['updated_at'] = date("H:i:s Y-m-d", strtotime($l['updated_at']));
            $data[] = $item;
        }
        return $item;
    }

    // get all users in team
    private function getListUser(User $user)
    {
        $data = array();
        $list = User::where('deleted', '=', 0)->where('team_id', '=', $user->team_id)->orderBy('role_id')->orderBy('firstName')->get();
        foreach ($list as $l) {
            $item['id'] = $l['id'];
            $item['username'] = $l['username'];
            $item['password'] = $l['password'];
            $item['email'] = $l['email'];
            $item['firstName'] = $l['firstName'];
            $item['lastName'] = $l['lastName'];
            $item['dateOfBirth'] = date("Y-m-d", strtotime($l['dateOfBirth']));
            $item['phone'] = $l['phone'];
            $item['avataUrl'] = $l['avataUrl'];
            $item['team_id'] = $l['team_id'];
            $item['jobTitle'] = $l['jobTitle'];
            $item['role_id'] = $l['role_id'];
            $item['created_at'] = date("H:i:s Y-m-d", strtotime($l['created_at']));
            $item['updated_at'] = date("H:i:s Y-m-d", strtotime($l['updated_at']));

            $data[] = $item;
        }
        return $data;
    }

    // get all lists
    private function getAppList(User $user)
    {
        $data = array();
        // get shiftwork list in my team
        $shiftwork = Shiftwork::where('deleted', '=', 0)->where('team_id', '=', $user->team_id)->orderBy('created_at')->get();
        $i = 0;
        foreach ($shiftwork as $l) {
            $data['list_shiftwork'][$i] = $l['shiftworkName'];
            $i++;
        }

        return $data;
    }

    private function uploadImage($object, $img, $action)
    {
        $name = substr(str_shuffle('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, 4);
        if ($action == 'created') {
            file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/CheckinApi/public/uploads/' . $object . $name . '.png', base64_decode($img));
            return $this->dir . $object . $name . '.png';
        }
    }
}
