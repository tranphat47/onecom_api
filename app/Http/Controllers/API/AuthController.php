<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\ResponseController as ResponseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\User;

class AuthController extends ResponseController
{
    public function change_password(Request $request)
    {
        $input = $request->all();
        $userid = Auth::guard('api')->user()->id;
        $rules = array(
            'old_password' => 'required',
            'new_password' => 'required|min:6',
        );
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $response = array('status' => 400, 'message' => $validator->errors()->first(), 'data' => array());
        } else {
            try {
                if ((Hash::check(request('old_password'), Auth::user()->password)) == false) {
                    $response = array('status' => 400, 'message' => 'Check your old password.', 'data' => array());
                } else if ((Hash::check(request('new_password'), Auth::user()->password)) == true) {
                    $response = array('status' => 400, 'message' => 'Please enter a password which is not similar then current password.', 'data' => array());
                } else {
                    User::where('id', $userid)->update(['password' => Hash::make($input['new_password'])]);
                    $response = array('status' => 200, 'message' => 'Password updated successfully.', 'data' => array());
                }
            } catch (\Exception $ex) {
                if (isset($ex->errorInfo[2])) {
                    $msg = $ex->errorInfo[2];
                } else {
                    $msg = $ex->getMessage();
                }
                $response = array('status' => 400, 'message' => $msg, 'data' => array());
            }
        }

        return response()->json($response);
    }
}
