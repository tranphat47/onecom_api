<?php

namespace App\Http\Controllers\API;


use Illuminate\Http\Request;
use App\Http\Controllers\API\ResponseController as ResponseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\News;
use App\Shiftwork;
use Illuminate\Support\Facades\Validator;

class SecondApiController extends ResponseController
{
    public $successStatus = 200;

    private $dir = 'http://huuhieu.name.vn/CheckinApi/public/uploads/';

    //----- NEWS
    public function createNews(Request $rq)
    {
        $rq['id'] = substr(str_shuffle('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, 20);
        if($rq['imgUrl']!=null){
            $rq['imgUrl'] = $this->uploadImage($rq['id'],$rq['imgUrl'],'created');
        }
        $news = News::create([
            'id' => $rq['id'],
            'title' => $rq['title'],
            'description' => $rq['description'],
            'url' => $rq['url'],
            'imgUrl' => $rq['imgUrl'],
            'createBy' => $rq['createBy'],
            'team_id' => $rq['team_id'],
        ]);
        if($news){
            $data['success'] = true;
            $data['message'] = "Successful!";
        }
        else{
            $data['success'] = false;
            $data['message'] = "Failed! Please try again!";
        }
        return response()->json($data, $this->successStatus);
    }

    public function updateNews (Request $rq)
    {
        if($rq['imgUrl']!=null){
            $rq['imgUrl'] = $this->uploadImage($rq['id'],$rq['imgUrl'],'created');
        }
        $item = News::where('id',$rq['id'])->update($rq->all());
        if($item){
            $data['success'] = true;
            $data['message'] = "Successful!";
        }
        else{
            $data['success'] = false;
            $data['message'] = "Failed! Please try again!";
        }
        return response()->json($data, $this->successStatus);
    }

    public function deleteNews (Request $rq)
    {
        $item = News::where('id',$rq['id'])->update([
            'deleted' => 1,
        ]);
        if($item){
            $data['success'] = true;
            $data['message'] = "Successful!";
        }
        else{
            $data['success'] = false;
            $data['message'] = "Failed! Please try again!";
        }
        return response()->json($data, $this->successStatus);
    }

    //----- SHIFTWORK
    public function createShiftwork(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'shiftworkName' => 'required|string|',
            'begin' => 'required',
            'end' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError($validator->errors());
        }

        $input = $request->all();
        $input['id'] = substr(str_shuffle('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, 20);

        $shiftwork = Shiftwork::create($input);
        if($shiftwork){
            $data['success'] = true;
            $data['message'] = "Successful!";
        }
        else{
            $data['success'] = false;
            $data['message'] = "Failed! Please try again!";
        }
        return response()->json($data, $this->successStatus);
    }

    public function updateShiftwork (Request $rq)
    {
        $item = Shiftwork::where('id',$rq['id'])->update($rq->all());
        // $item->save();
        if($item){
            $data['success'] = true;
            $data['message'] = 'Successful!';
        }
        else{
            $data['success'] = false;
            $data['message'] = "Failed! Please try again!";
        }
        return response()->json($data, $this->successStatus);
    }

    public function deleteShiftwork (Request $rq)
    {
        $item = Shiftwork::where('deleted','=',0)->where('id','=',$rq['id'])->update([
            'deleted' => 1,
        ]);
        if($item){
            $data['success'] = true;
            $data['message'] = "Successful!";
        }
        else{
            $data['success'] = false;
            $data['message'] = "Failed! Please try again!";
        }
        return response()->json($data, $this->successStatus);
    }

    private  function uploadImage($object, $img,$action){
        $name = substr(str_shuffle('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, 4);
        if($action == 'created' ){
            file_put_contents($_SERVER['DOCUMENT_ROOT'].'/CheckinApi/public/uploads/'.$object.$name.'.png', base64_decode($img));
            return $this->dir.$object.$name.'.png';
        }
    }
}
