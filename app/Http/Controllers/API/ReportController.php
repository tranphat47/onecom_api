<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\ResponseController as ResponseController;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Registration;
use App\User;

class ReportController extends ResponseController
{
    public function getAllReport(Request $request)
    {
        if ($request->from && $request->to) {
            $from = Carbon::parse($request->from)->startOfDay()->toDateTimeString();
            $to = Carbon::parse($request->to)->endOfDay()->toDateTimeString();
            $users = ($request->team_id)
                ? User::where('team_id', $request->team_id)->get()
                : User::all();

            $response = [];
            foreach ($users as $user) {
                $sum = 0;
                foreach ($user->getRegistrationInRange($from, $to) as $item) {
                    if ($item->isCheckin == 1 && $item->isCheckout == 1)
                        $sum += ($item->shiftworkName == "All Day") ? 1 : 0.5;
                }

                $dt['fullName'] = $user->full_name;
                $dt['totalDay'] = $sum;
                $dt['dateOfBirth'] = Carbon::create($user->dateOfBirth)->toDateString();
                $dt['jobTitle'] = $user->jobTitle;
                $response[] = $dt;
            }
            if ($request->sort == '1') {
                usort($response, function ($a, $b) {
                    return $a['totalDay'] < $b['totalDay'];
                });
            }
            return response()->json(["report" => $response]);
        } else {
            return response()->json(["error" => "Missing Paramater"]);
        }
    }

    public function getReport(Request $request)
    {
        if ($request->from && $request->to) {
            $list = [];
            $from = Carbon::parse($request->from)->startOfDay()->toDateTimeString();
            $to = Carbon::parse($request->to)->endOfDay()->toDateTimeString();
            $data = Registration::whereBetween('date', [$from, $to])->get()->groupBy('user_id');
            $response = [];
            foreach ($data as $user_id => $workshifts) {
                $sum = 0;
                foreach ($workshifts as $item) {
                    if ($item->isCheckin == 1 && $item->isCheckout == 1)
                        $sum += ($item->shiftworkName == "All Day") ? 1 : 0.5;
                }

                $user = User::find($user_id);
                $dt['fullName'] = $user->full_name;
                $dt['totalDay'] = $sum;
                $dt['dateOfBirth'] = Carbon::create($user->dateOfBirth)->toDateString();
                $dt['jobTitle'] = $user->jobTitle;
                $list[] = $dt;
                $response['report'] = $list;
            }
            return response()->json($response);
        } else {
            return response()->json(["error" => "Missing Paramater"]);
        }
    }
}
