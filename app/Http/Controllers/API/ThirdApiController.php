<?php

namespace App\Http\Controllers\API;


use App\News;
use App\Registration;
use App\Shiftwork;
use Illuminate\Http\Request;
use App\Http\Controllers\API\ResponseController as ResponseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Team;
use Illuminate\Support\Facades\Validator;

class ThirdApiController extends ResponseController
{
    public $successStatus = 200;

    private $dir = 'http://huuhieu.name.vn/CheckinApi/public/uploads/';

    //----- TEAM
    public function createTeam(Request $rq)
    {
        $validator = Validator::make($rq->all(), [
            'username' => 'required|string|',
//            'adminEmail' => 'required|string|email',
            'password' => 'required',
//            'confirm_password' => 'required|same:password',
//            'teamName' => 'required|string|',
//            'teamEmail'=>'required|string|email'
        ]);

        if ($validator->fails()) {
            return $this->sendError($validator->errors());
        }
        $userExist = User::where('username', $rq['username'])->where('deleted', 0)->first();
        if ($userExist) {
            $data['success'] = false;
            $data['message'] = "Failed! User already exists";
        } else {
            $id_team = substr(str_shuffle('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, 20);
            $team = Team::create([
                'id' => $id_team,
                'teamName' => $rq['teamName'],
                'teamCode' => $rq['teamCode'],
//            'email' => $rq['teamEmail'],
//            'phone' => $rq['teamPhone'],
//            'address' => $rq['address'],
//            'timeLimit' => $rq['timeLimit'],
//            'isWorkOnSat' => $rq['isWorkOnSat'],
//            'ipWifi' => $rq['ipWifi']
            ]);

            $user = User::create([
                'id' => substr(str_shuffle('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, 20),
                'username' => $rq['username'],
                'password' => bcrypt($rq['password']),
//            'email' => $rq['email'],
//            'phone' => $rq['phone'],
                'firstName' => $rq['firstName'],
                'lastName' => $rq['lastName'],
//            'dateOfBirth' => date('Y-m-d',strtotime($rq['dateOfBirth'])),
                'role_id' => '1',
                'team_id' => $id_team,
//            'jobTitle' => $rq['jobTitle']
            ]);
            $shiftwork_number = 0;
            for ($i = 1; $i <= 3; $i++) {
                if ($i == 1) $shiftwork_name = 'Morning shiftwork';
                if ($i == 2) $shiftwork_name = 'Afternoon shiftwork';
                if ($i == 3) $shiftwork_name = 'All Day';
                $shiftwork = Shiftwork::create([
                    'id' => substr(str_shuffle('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, 20),
                    'shiftworkName' => $shiftwork_name,
                    'team_id' => $id_team,
                ]);
                $shiftwork_number = $i;
                sleep(1);
            }
            if ($shiftwork_number && $shiftwork)
                $data['shiftwork'] = "Create " . $shiftwork_number . " shiftwork successfully!";
            else  $data['shiftwork'] = "Create shiftwork failed!";

            if ($user && $team) {
                $data['token'] = $user->createToken('token')->accessToken;
                $data['success'] = true;
                $data['message'] = "Successful!";
            } else {
                $data['success'] = false;
                $data['message'] = "Failed! Please try again!";
            }
        }
        return response()->json($data, $this->successStatus);
    }

    public function updateTeam (Request $rq)
    {
        $item = Team::where('id',$rq['id'])->update($rq->all());
        if($item){
            $data['success'] = true;
            $data['message'] = "Successful!";
        }
        else{
            $data['success'] = false;
            $data['message'] = "Failed! Please try again!";
        }
        return response()->json($data, $this->successStatus);
    }

    public function deleteTeam (Request $rq)
    {
        $user_ = User::where('team_id','=',$rq['id'])->chunkById(100, function ($users) {
            foreach ($users as $user) {
                DB::table('users')
                    ->where('id', $user->id)
                    ->update(['deleted' => 1]);
                DB::table('registrations')
                    ->where('user_id', $user->id)
                    ->update(['deleted' => 1]);
            }
        });
        $news = News::where('team_id','=',$rq['id'])->update([
            'deleted' => 1,
        ]);
        $shiftwork = Shiftwork::where('team_id','=',$rq['id'])->update([
            'deleted' => 1,
        ]);
        $team = Team::where('id',$rq['id'])->update([
            'deleted' => 1,
        ]);
        if($user_ && $team && $news && $shiftwork){
            $data['success'] = true;
            $data['message'] = "Successful!";
        }
        else{
            $data['success'] = false;
            $data['message'] = "Failed! Please try again!";
        }
        return response()->json($data, $this->successStatus);
    }

    //test commit branch 
}
