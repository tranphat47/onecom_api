<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shiftwork extends Model
{
    protected $fillable = [
        'id', 'shiftworkName', 'begin','end','team_id', 'deleted', 'created_at', 'updated_at',
    ];

    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';

}
