<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Registration extends Model
{
    protected $fillable = [
        'id', 'shiftwork_id', 'shiftworkName', 'user_id', 'date',
        'isCheckin', 'isCheckout', 'isPresent', 'atCompany', 'deleted', 'created_at', 'updated_at',
    ];

    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';

    public function shiftwork()
    {
        return $this->hasOne('App\Shiftwork');
    }
}
