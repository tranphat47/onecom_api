<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens,Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'username', 'password', 'firstName', 'lastName', 'dateOfBirth', 'phone', 'avataUrl', 'email','role_id', 'team_id','jobTitle', 'deleted', 'created_at', 'updated_at',
    ];

    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function employee()
    {
        return $this->hasOne('App\Employee', 'username','username');
    }

    public function registrations()
    {
        return $this->hasMany('App\Registration');
    }

    public function getFullNameAttribute() {
        return "{$this->firstName} {$this->lastName}";
    }

    public function getRegistrationInRange($from, $to) {
        return $this->registrations->whereBetween('date', [$from, $to]);
    }

}
