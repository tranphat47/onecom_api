<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $fillable = [
        'id', 'teamName', 'teamCode','email','phone', 'address','timeLimit','isWorkOnSat','ipWifi', 'deleted', 'created_at', 'updated_at',
    ];

    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';

    public function news()
    {
        return $this->hasMany('App\News', 'team_id', 'id');
    }
    public function user()
    {
        return $this->hasMany('App\User', 'team_id', 'id');
    }
}
