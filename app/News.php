<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $fillable = [
        'id', 'title', 'description', 'url', 'imgUrl', 'createBy', 'team_id', 'deleted', 'created_at', 'updated_at',
    ];

    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';
}
