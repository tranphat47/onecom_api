-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1:3306
-- Thời gian đã tạo: Th6 04, 2020 lúc 10:11 AM
-- Phiên bản máy phục vụ: 5.7.28
-- Phiên bản PHP: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `test_oc`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `news`
--

DROP TABLE IF EXISTS `news`;
CREATE TABLE IF NOT EXISTS `news` (
  `id` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `imgUrl` varchar(255) DEFAULT NULL,
  `createBy` varchar(255) DEFAULT NULL,
  `team_id` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_news_user` (`createBy`),
  KEY `FK_news_teams` (`team_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `news`
--

INSERT INTO `news` (`id`, `title`, `description`, `url`, `imgUrl`, `createBy`, `team_id`, `created_at`, `updated_at`, `deleted`) VALUES
('1', 'news demo', 'hahahhahha', NULL, NULL, '4', NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
CREATE TABLE IF NOT EXISTS `oauth_access_tokens` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('083eccfde7ebc325a57b903ea0b66127a3eb3e5c21a61b4fdfadccbc1cb7416421f2730a19f38d5b', 1, 1, 'token', '[]', 1, '2020-05-21 15:32:31', '2020-05-21 15:32:31', '2021-05-22 05:32:31'),
('4faa518ddbf8a7c3187de30ab1c2f2dbf290b5f100da56901f362ce447c1197f4fa0579894c69b15', 5, 1, 'MyApp', '[]', 0, '2020-05-29 03:59:37', '2020-05-29 03:59:37', '2021-05-29 10:59:37'),
('6b7b8ceb9a7686b32e3c61177349b5faa313a17dcc4a3030800d5de58570b26c77b4344584dbabf5', 9, 1, 'MyApp', '[]', 0, '2020-06-03 20:10:26', '2020-06-03 20:10:26', '2021-06-04 03:10:26'),
('8b824f597ba913f1660921d34d847dcc7a421d189336743439125b8a1ba8ff3df2e0e04409e67b8a', 1, 1, 'token', '[]', 0, '2020-05-21 15:30:58', '2020-05-21 15:30:58', '2021-05-22 05:30:58'),
('ac1e2f489463f8bd224de4086340f99174dac009c2f8775bf90d06c873a4edcec077c1b4db6b6810', 3, 1, 'MyApp', '[]', 0, '2020-05-26 20:39:26', '2020-05-26 20:39:26', '2021-05-27 10:39:26'),
('c309ba0ad2bd22ee803d8109cfb00c2fcf632ebbdb8dde44550a93287bac5a02e89543e11c9b1a2d', 4, 1, 'token', '[]', 0, '2020-05-26 21:00:13', '2020-05-26 21:00:13', '2021-05-27 11:00:13'),
('c3e34c89b74d5bf5aacb8fd6a15942e39a5e2798c8725e3a5c84b90a6e80d9d0368a4448f9e7c0db', 0, 1, 'token', '[]', 0, '2020-06-03 20:02:16', '2020-06-03 20:02:16', '2021-06-04 03:02:16'),
('c55ffc73e1dcb1cd41e917089ec30bccf78ca5928d40763859fd524ff7cce8fbe13a539f60077584', 5, 1, 'token', '[]', 0, '2020-05-27 21:47:18', '2020-05-27 21:47:18', '2021-05-28 11:47:18'),
('fb80166e1813c83c7ace8c30f5b7aefeb63bb7adbdbffe9c0fc5cca18469ab96300cf3cfdc07a0dc', 3, 1, 'token', '[]', 0, '2020-05-26 20:36:50', '2020-05-26 20:36:50', '2021-05-27 10:36:50');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
CREATE TABLE IF NOT EXISTS `oauth_auth_codes` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
CREATE TABLE IF NOT EXISTS `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', '3SnbxYc2sCMB3yZqS9JcxME34f2u3sGaXYX2BByM', 'http://localhost', 1, 0, 0, '2020-05-21 15:18:13', '2020-05-21 15:18:13'),
(2, NULL, 'Laravel Password Grant Client', 'qcwjrahgi2YjxV0h2vsH037nJbUwgLaX3Q3ezAn9', 'http://localhost', 0, 1, 0, '2020-05-21 15:18:13', '2020-05-21 15:18:13');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
CREATE TABLE IF NOT EXISTS `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-05-21 15:18:13', '2020-05-21 15:18:13');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
CREATE TABLE IF NOT EXISTS `oauth_refresh_tokens` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `registrations`
--

DROP TABLE IF EXISTS `registrations`;
CREATE TABLE IF NOT EXISTS `registrations` (
  `id` varchar(255) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `shiftwork_id` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `isCheckin` tinyint(1) DEFAULT '0',
  `isCheckout` tinyint(1) DEFAULT '0',
  `isPresent` tinyint(1) DEFAULT NULL,
  `atCompany` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_registrations_shiftworks` (`shiftwork_id`),
  KEY `FK_registrations_users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `registrations`
--

INSERT INTO `registrations` (`id`, `user_id`, `shiftwork_id`, `date`, `isCheckin`, `isCheckout`, `isPresent`, `atCompany`, `created_at`, `updated_at`, `deleted`) VALUES
('1', '4', '1', '2020-05-30 00:00:00', 1, 1, 1, 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` varchar(255) NOT NULL,
  `roleName` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `roles`
--

INSERT INTO `roles` (`id`, `roleName`, `created_at`, `updated_at`, `deleted`) VALUES
('1', 'admin', '2020-05-28 17:00:00', '2020-05-28 17:00:00', 0),
('2', 'user', NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `shiftworks`
--

DROP TABLE IF EXISTS `shiftworks`;
CREATE TABLE IF NOT EXISTS `shiftworks` (
  `id` varchar(255) NOT NULL,
  `shiftworkName` varchar(255) DEFAULT NULL,
  `begin` time DEFAULT NULL,
  `end` time DEFAULT NULL,
  `team_id` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_shiftworks_teams` (`team_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `shiftworks`
--

INSERT INTO `shiftworks` (`id`, `shiftworkName`, `begin`, `end`, `team_id`, `created_at`, `updated_at`, `deleted`) VALUES
('1', 'ca 1', '09:00:00', '12:00:00', '1', NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `teams`
--

DROP TABLE IF EXISTS `teams`;
CREATE TABLE IF NOT EXISTS `teams` (
  `id` varchar(255) NOT NULL,
  `teamName` varchar(255) DEFAULT NULL,
  `teamCode` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `timeLimit` int(10) DEFAULT NULL,
  `isWorkOnSat` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `teams`
--

INSERT INTO `teams` (`id`, `teamName`, `teamCode`, `email`, `phone`, `address`, `timeLimit`, `isWorkOnSat`, `created_at`, `updated_at`, `deleted`) VALUES
('1', 'team demo', 'demo', 'demo@gmail.com', '0123456789', 'demo', 30, 1, '2020-05-28 17:00:00', '2020-05-28 17:00:00', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` varchar(255) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `firstName` varchar(255) DEFAULT NULL,
  `dateOfBirth` datetime DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `avataUrl` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  `role_id` varchar(255) DEFAULT '2',
  `team_id` varchar(255) DEFAULT NULL,
  `jobTitle` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `FK_users_role` (`role_id`),
  KEY `FK_users_teams` (`team_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `lastName`, `firstName`, `dateOfBirth`, `phone`, `avataUrl`, `email`, `email_verified_at`, `remember_token`, `role_id`, `team_id`, `jobTitle`, `deleted`, `created_at`, `updated_at`) VALUES
('3', 'user1', '$2y$10$5Yad8UwkbFeVFk8R5kQe5eM3DUXjlF7EFBaxXCyrrOEyWoGDkW/fa', NULL, NULL, NULL, NULL, NULL, 'user1@gmail.com', NULL, NULL, '2', '1', NULL, 0, '2020-05-26 13:36:49', '2020-05-26 13:36:49'),
('4', 'admin', '$2y$10$SljDd5jZas2qHCSDy74aYOKJ/taFtPasiaCYxy2AMYloVX/Gw0dQq', NULL, NULL, NULL, NULL, NULL, 'admin@gmail.com', NULL, NULL, '1', '1', NULL, 0, '2020-05-26 14:00:13', '2020-05-26 14:00:13'),
('5', 'user2', '$2y$10$EDcf5RoFtvnzjzMytOOlY.JTV/.T6bh9CI6Zj/ELw3JK2fCLdrAYS', NULL, NULL, NULL, NULL, NULL, 'ngocngoc@gmail.com', NULL, NULL, '2', '1', NULL, 0, '2020-05-27 14:47:18', '2020-05-27 14:47:18'),
('9RWI0TMjdG18UmuFlBwb', 'user10', '$2y$10$BDad5uP93SsdE5jm6lKga.t5XxiSaJOPyuehE.kDmXoQHdNu6s2Gu', 'demo', NULL, '1999-05-05 00:00:00', '0123546789', NULL, 'user10@gmail.com', NULL, NULL, '2', '1', NULL, 0, '2020-06-03 20:02:16', '2020-06-03 20:02:16');

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `FK_news_teams` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`),
  ADD CONSTRAINT `FK_news_user` FOREIGN KEY (`createBy`) REFERENCES `users` (`id`);

--
-- Các ràng buộc cho bảng `registrations`
--
ALTER TABLE `registrations`
  ADD CONSTRAINT `FK_registrations_shiftworks` FOREIGN KEY (`shiftwork_id`) REFERENCES `shiftworks` (`id`),
  ADD CONSTRAINT `FK_registrations_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Các ràng buộc cho bảng `shiftworks`
--
ALTER TABLE `shiftworks`
  ADD CONSTRAINT `FK_shiftworks_teams` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`);

--
-- Các ràng buộc cho bảng `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `FK_users_role` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  ADD CONSTRAINT `FK_users_teams` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
