<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/ccc', function () {
    var_dump(DB::connection()->getDatabaseName());
    die();
});

Route::post('login', 'API\FirstApiController@login');
Route::post('create_team', 'API\ThirdApiController@createTeam');

//Route::post('register', 'API\FirstApiController@register');

Route::group(['middleware' => ['auth:api']], function () {

    Route::get('logout', 'API\FirstApiController@logout');
    Route::get('get_info_user', 'API\FirstApiController@getInfoUser');
    Route::get('get_registration_today', 'API\FirstApiController@getRegistrationToday');

    //user
    Route::post('create_user', 'API\FirstApiController@createUser');
    Route::post('update_user/{user}', 'API\FirstApiController@updateUser');
    Route::post('delete_user/{user}', 'API\FirstApiController@deleteUser');
    Route::post('change_password/{user}', 'API\FirstApiController@changePassword');

    //registration
    Route::post('create_registration', 'API\FirstApiController@createRegistration');
    Route::post('update_registration/{registration}', 'API\FirstApiController@updateRegistration');
    Route::post('delete_registration/{registration}', 'API\FirstApiController@deleteRegistration');
    Route::post('create_registration_allday', 'API\FirstApiController@createRegistrationAllday');
    Route::post('delete_registration_allday', 'API\FirstApiController@deleteRegistrationAllday');

    //news
    Route::post('create_news', 'API\SecondApiController@createNews');
    Route::post('update_news/{news}', 'API\SecondApiController@updateNews');
    Route::post('delete_news/{news}', 'API\SecondApiController@deleteNews');

    //shiftwork
    Route::post('create_shiftwork', 'API\SecondApiController@createShiftwork');
    Route::post('update_shiftwork/{shiftwork}', 'API\SecondApiController@updateShiftwork');
    Route::post('delete_shiftwork/{shiftwork}', 'API\SecondApiController@deleteShiftwork');

    //team
    Route::post('update_team/{team}', 'API\ThirdApiController@updateTeam');
    Route::post('delete_team/{team}', 'API\ThirdApiController@deleteTeam');
    Route::post('update_timelimit/{team}', 'API\FirstApiController@updateTimeLimit');

    // Report
    Route::post('report', 'API\ReportController@getAllReport');

    // Reset Password
    Route::post('change_password', 'API\AuthController@change_password');
});

